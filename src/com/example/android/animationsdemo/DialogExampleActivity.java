/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.animationsdemo;



import android.support.v7.app.*;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;




public class DialogExampleActivity extends FragmentActivity implements CoolDialogFragment.CoolDialogListener {

    Button mButtonDialog;
    TextView mTextView;
    int mClickNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_example);

        mClickNum = 0;

        mTextView = (TextView) findViewById(R.id.showTextView);

        //this will register this view for a context menu now. you need to long click this
        registerForContextMenu(mTextView);

        mButtonDialog = (Button) findViewById(R.id.button_dialog);
        mButtonDialog.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mClickNum = 0; //reset
                mTextView.setText("");
                fireDialog();
            }
        });

    }

    //this will inflate the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dialog_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //this will provide behavior for them.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {

            case R.id.act_one:
                Toast.makeText(this,"one selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.act_two:
                Toast.makeText(this,"two selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.act_three:
                Toast.makeText(this,"three selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.act_four:
                Toast.makeText(this,"four selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.act_five:
                Toast.makeText(this,"five selected", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //this will inflate the context menu
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_actions, menu);
    }

    //this will define behavior for the context menu
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.act_edit:
                Toast.makeText(this,"edit selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.act_delete:
                Toast.makeText(this,"delete selected", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


    public void fireDialog() {

        DialogFragment dialog = new CoolDialogFragment();
        dialog.show(getSupportFragmentManager(), "Dialog");

    }

    @Override
    public void onDialogIncrementClick(DialogFragment dialog) {

        mClickNum++;
        fireDialog();
    }

    @Override
    public void onDialogQuitClick(DialogFragment dialog) {

        mTextView.setText("You clicked : " + mClickNum + " times.");

    }
}
